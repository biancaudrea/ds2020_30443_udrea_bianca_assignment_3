package ro.tuc.ds2020client.controller;

import com.google.gson.Gson;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import ro.tuc.ds2020client.model.MedicationPlanSerializable;
import ro.tuc.ds2020client.service.MedicationPlanServiceRmi;
import ro.tuc.ds2020client.swing.SwingApp;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Timer;
import java.util.TimerTask;


public class MainController {
    private MedicationPlanServiceRmi medicationPlanServiceRmi;
    private DateTimeFormatter formatter= DateTimeFormatter.ofPattern("HH:mm");
    private String patientId = "d52c0aa0-d29b-479f-afda-15e3cc2d5166";
    private MedicationPlanSerializable medicationPlanSerializable;
    private static final int intakePeriod = 6;
    private LocalDateTime startTime;
    private SwingApp swingApp;
    public MainController(MedicationPlanServiceRmi medicationPlanServiceRmi) throws Exception {
        this.medicationPlanServiceRmi = medicationPlanServiceRmi;
        swingApp = new SwingApp();
        medicationPlanSerializable = getMedication();
        swingApp.addMedicationPlan(medicationPlanSerializable);
        start();
    }

    public MedicationPlanSerializable getMedication() throws Exception {
        Gson gson = new Gson();
        medicationPlanSerializable = gson.fromJson( medicationPlanServiceRmi.getMedicationPlan(patientId), MedicationPlanSerializable.class);
       swingApp.addMedicationPlan(medicationPlanSerializable);
        return medicationPlanSerializable;
    }


    public void start() throws Exception {

        startTime = LocalDateTime.now();

        //////////////
        //daily scheduler for taking medication plan from server
        /////////////
        Timer timer = new Timer ();
        TimerTask dailytask = new TimerTask () {
            @Override
            public void run () {
                Gson gson = new Gson();
                try {
                    medicationPlanSerializable = gson.fromJson( medicationPlanServiceRmi.getMedicationPlan(patientId), MedicationPlanSerializable.class);
                    swingApp.addMedicationPlan(medicationPlanSerializable);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        };

        timer.schedule (dailytask, 0l, DateUtils.MILLIS_PER_DAY);

        //////////
        //scheduler every intake hours for updating table content
        //////////
        Timer timer1 = new Timer ();
        TimerTask getNewTableContent = new TimerTask () {
            @Override
            public void run () {
                swingApp.addMedicationPlan(medicationPlanSerializable);
                System.out.println("Table content has been updated");
            }
        };

        timer.schedule (getNewTableContent, 0l, DateUtils.MILLIS_PER_HOUR * intakePeriod);


        //////////
        //scheduler every intake hours for updating table content
        //////////
        Timer timer2 = new Timer ();
        TimerTask timerEveryMinute = new TimerTask () {
            @Override
            public void run () {
                if(!swingApp.isTableEmpty() && startTime.plusMinutes(5).isBefore(LocalDateTime.now())) {
                    swingApp.deleteRow(0);
                    medicationPlanServiceRmi.medicationTaken(false, medicationPlanSerializable.getId());
                    System.out.println("Medicine not taken");
                }
                swingApp.setTime(LocalDateTime.now().format(formatter));
            }
        };

        timer.schedule (timerEveryMinute, 0l, DateUtils.MILLIS_PER_MINUTE);


        swingApp.addTakenButtonListener(e->{
            medicationPlanServiceRmi.medicationTaken(true, medicationPlanSerializable.getId());
            swingApp.deleteRow(0);
            System.out.println("Medicine taken");
        });


    }
}
