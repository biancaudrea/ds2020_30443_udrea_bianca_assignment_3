package ro.tuc.ds2020client.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

import ro.tuc.ds2020client.service.MedicationPlanServiceRmi;
import ro.tuc.ds2020client.swing.SwingApp;

@Configuration
public class Client {


    @Bean
    public HttpInvokerProxyFactoryBean invoker() {
        HttpInvokerProxyFactoryBean invoker = new HttpInvokerProxyFactoryBean();
        invoker.setServiceUrl("http://localhost:8080/booking");
        invoker.setServiceInterface(MedicationPlanServiceRmi.class);
        return invoker;
    }


}
