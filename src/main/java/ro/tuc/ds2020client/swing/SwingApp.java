package ro.tuc.ds2020client.swing;

import org.apache.tomcat.jni.Local;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020client.model.MedicationPlanSerializable;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


public class SwingApp extends JFrame {

    public SwingApp() {
        initUI();
    }

    private JLabel label8;
    private JLabel label9;
    private JLabel label10;
    private JLabel label11;
    private JLabel label12;
    private JLabel label1;

    DefaultTableModel model;

    private JButton takenButton;
    private JTable table;
    private String[][] data=new String[60][4];


    public void initUI (){

        setBounds(480, 20, 1200, 700);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        getContentPane().setBackground(Color.lightGray);

        label11 = new JLabel("Welcome back! These are you medication plans:");
        label11.setBounds(150, 20, 1100, 45);
        getContentPane().add(label11);
        label11.setFont(new Font("Courier", Font.BOLD,38));


        label8 = new JLabel("Name");
        label8.setBounds(80, 120, 500, 17);
        getContentPane().add(label8);
        label8.setFont(new Font("Courier", Font.BOLD,17));

        label9 = new JLabel("Intake start");
        label9.setBounds(245, 120, 500, 17);
        getContentPane().add(label9);
        label9.setFont(new Font("Courier", Font.BOLD,17));

        label10 = new JLabel("Intake end");
        label10.setBounds(410, 120, 500, 17);
        getContentPane().add(label10);
        label10.setFont(new Font("Courier", Font.BOLD,17));

        label12 = new JLabel("Period (days)");
        label12.setBounds(590, 120, 500, 17);
        getContentPane().add(label12);
        label12.setFont(new Font("Courier", Font.BOLD,17));



        String[] column = { "name","intakeStart","intakeEnd","period"};
        model = new DefaultTableModel(data, column);
        table = new JTable(model);

        table.setBounds(15, 150, 700, 400);

        JScrollPane scrollPane = new JScrollPane();
        table.add(scrollPane);

        getContentPane().add(table);

        takenButton= new JButton("Taken");
        takenButton.setBounds(800, 200, 100, 70);
        getContentPane().add(takenButton);
        takenButton.setFont(new Font("Courier", Font.BOLD,17));

        label1 = new JLabel("");
        label1.setBounds(800, 400, 1100, 65);
        getContentPane().add(label1);
        label1.setFont(new Font("Courier", Font.BOLD,60));

        setVisible(true);
    }

    public void setTableContent(String[][] newData)
    {
        for(int i=0;i<newData.length;i++)
            for(int j=0;j<4;j++)
                table.setValueAt(newData[i][j],i,j);
    }

    public void addMedicationPlan(MedicationPlanSerializable medicationPlanSerializable){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        LocalDateTime startTime = LocalDateTime.now();
        String startDate = LocalDateTime.now().format(formatter);
        String endDate = LocalDateTime.now().plusMinutes(5).format(formatter);
        String[][] string = new String[10][4];
        string[0][0] = medicationPlanSerializable.getMedicationName();
        string[0][1]= startDate;
        string[0][2]= endDate;
        string[0][3]=String.valueOf(medicationPlanSerializable.getPeriod());
        setTableContent(string);
    }

    public void addTakenButtonListener(final ActionListener actionListener) {
        takenButton.addActionListener(actionListener);
    }

    public Object getSelectedId()
    {
        int selectedRow = table.getSelectedRow();
        Object selectedCellValue=table.getValueAt(selectedRow, 0);
        return selectedCellValue;
    }
    public void addMedicationToTable(MedicationPlanSerializable medicationPlan)
    {
        model.addRow(new Object[]{medicationPlan.getMedicationName(), medicationPlan.getIntakeIntervals(), medicationPlan.getIntakeIntervals()+1, medicationPlan.getPeriod() });
    }
    public int getSelectedRow()
    {
        return table.getSelectedRow();
    }
    public int getSelectedColumn()
    {
        return table.getSelectedColumn();
    }

    public boolean isTableEmpty(){
        return table.getValueAt(0,0)=="";
    }

    public void displayMessage(String message,String title)
    {
        JOptionPane.showMessageDialog(null, message, title, JOptionPane.INFORMATION_MESSAGE, null);
    }

    public void deleteRow(int row)
    {
       // model.removeRow(row);
        for(int j=0;j<4;j++)
            table.setValueAt("",row,j);
    }
    public void setTime(String time){
        label1.setText(time);
    }
}
