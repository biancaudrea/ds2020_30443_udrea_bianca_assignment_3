package ro.tuc.ds2020client;

import com.google.gson.Gson;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import ro.tuc.ds2020client.controller.MainController;
import ro.tuc.ds2020client.model.MedicationPlanSerializable;
import ro.tuc.ds2020client.service.MedicationPlanServiceRmi;
import ro.tuc.ds2020client.swing.SwingApp;


import java.awt.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class Ds2020ClientApplication {


	public static void main(String[] args) throws Exception {

		SpringApplicationBuilder builder = new SpringApplicationBuilder(Ds2020ClientApplication.class);

		builder.headless(false);

		MedicationPlanServiceRmi medicationPlanServiceRmi = builder.run(args).getBean(MedicationPlanServiceRmi.class);

		new MainController(medicationPlanServiceRmi).start();

	}

}
