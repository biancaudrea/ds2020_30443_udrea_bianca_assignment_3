package ro.tuc.ds2020client.service;


import ro.tuc.ds2020client.model.MedicationPlanSerializable;

public interface MedicationPlanServiceRmi {
    String getMedicationPlan(String patientId) throws Exception;
    String medicationTaken(boolean taken,  String medicationPlanId);

}
